#!/bin/bash

device=/dev/video0

echo "v4l2-ctl"
v4l2-ctl --device $device --list-formats-ext > v4l2-ctl.formats
v4l2-ctl --device $device --all > v4l2-ctl.info-all
echo "lsusb"
lsusb -v -d 1bcf:2c99 > lsusb
echo "arecord"
arecord -L > arecord
