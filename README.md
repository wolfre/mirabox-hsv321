# MiraBox HSV321 

A quick test and review of the [MiraBox HSV321](https://www.amazon.de/-/en/MiraBox-HDMI-Game-Capture-Card/dp/B07G84G7VF) USB HDMI capture device on a Linux PC.

## Setup

Video source is a Wii set to 480p mode, connected to an OSSC via component cables.
OSSC HDMI output is fed directly to the HSV321 with a 3 meter HDMI cable.
The HDMI output from the HSV321 is connected to my usual TV for monitoring.
The MiraBox is connected to a SuperSpeed USB 3 port directly on the mainboard (using the USB cable supplied with the box).
The test PC is running Kubuntu 20.04, `x86_64` / `5.4.0-109-generic` Kernel.
The MiraBox was bought in April 2022.

## General information

The box is recognized as `1bcf:2c99 Sunplus Innovation Technology Inc. MiraBox Video Capture` and usable without any additional steps (see [full lsusb output here](lsusb)).
The HDMI pass-through is working without any further steps from the PC side.
Also there is no noticible lag on the pass-through, however I did not do a formal lag test.

The box offers the following resolutions, all of them for 30 fps and 60 fps, and in both Motion-JPEG and YUYV 4:2:2 (see [full details here](v4l2-ctl.info-all))
- 640x480
- 800x600
- 1024x768
- 1280x720
- 1280x960
- 1280x1024
- 1360x768
- 1400x900
- 1440x900
- 1920x1080

Note that it does not support 24 fps nor 25 fps nor 50 fps as an output option.
However a quick test with a 24 fps source (Raspberry Pi 4 with Kodi / [LibreELEC](https://libreelec.tv)) grabbing at 30 fps and 60 fps from the box worked.
In this case it does duplicate frames as needed (a sort of pull down).
Also note there is no 720x480 option, so it should match VGA 640x480 timings (*no* actually, see below), but not DTV 480p (like the Wii in this case).

The device offers 4 settings to adjust (see [full details here](v4l2-ctl.info-all))
- brightness (256 steps, default at 50%)
- contrast (256 steps, default at 50%)
- saturation (256 steps, default at 50%)
- hue (64 steps, default at 50%)

The box also registers as an audio source with multiple options (see [full details here](arecord)).
The two most notable are:
- Regular PCM device
- IEC958 (S/PDIF)

This suggests that (legacy) compressed streams such as Dolby Digital and DTS could be captured but not the newer HD audio formats.
However only PCM was tested (coming from the OSSC) and worked as expected (see below).

See also [get-info.sh](get-info.sh) on how the information was gathered.

## Image quality

Several still images were captured to test basic image acquisition of the box.
For the full list and unedited snaps go [over here](image-captures.md).
Unless noted otherwise, captures were taken with YUYV 4:2:2 and with all settings set to manufacturer default.
Also DVI over HDMI is supported by the MiraBox and yields the same image quality results as RGB.

Starting with the OSSC test pattern, here's the 480p DTV image captured with 640x480:

![640x480](ossc-testpattern-rgb-yuyv422-640x480.png "640x480")

The number of pixels in a line (640) does not match DTV timings (720), so some aliasing occurs (horizontal "shimmering" / wave patterns in the checkerboard borders), this was expected.
However there also is a similar pattern in vertical direction, which is unexpected as the pixel count matches.

Next let's have a closer look at a 1080p capture of the same OSSC test pattern.

![1920x1080 200%](ossc-testpattern-rgb-yuyv422-1920x1080-excerpt-200pct.png "1920x1080 200%")

This shows heavy ringing around color borders and also heavy color bleed (note the image section was 200% enlarged using nearest neighbor).
The color bleed should not come from the YUV 4:2:2 here as this applies to the transport from the box to the PC, but content is already the upscaled 1080p image.

A quick test with a 1080p source and a capture at 1080p also shows ringing / halos on contrast edges (note picture is missing).
So even if the HDMI input resolution matches the selected capture resolution, there seems to be some scaling / filtering involved.

The gradient in the middle of the test pattern also shows quite some deviation from neutral gray, as well as banding.

![YCbCr vs RGB OSSC](ossc-testpattern-ycbcr-vs-rgb-yuyv422-1920x1080.png "YCbCr vs RGB OSSC")

The above comparison shows the difference between RGB and YCbCr transmission over HDMI.
Doing the the same capture from RGB and using the Motion-JPEG transport format yielded the same results as YUV 4:2:2 (with additional JPEG compression artifacts).
To further compare results between these two color encodings, the following comparison shows the Wii settings menu.


![YCbCr vs RGB Wii menu](wii-menu-rgb-vs-ycbcr-yuyv422-1920x1080.png "YCbCr vs RGB Wii menu")

The settings menu has a good dynamic range (rather dark background with bright menu elements) and also offers some color elements.
To further check for color reproduction a pink (R=100%,G=0%,B=100%) border was added with the OSSC mask feature.
The difference is quite obvious, with the YCbCr being too dark, but the RGB capture also not 100% matching the intended colors (pink border and blue OSSC menu).

Finally comparing 2 different images with the 2 different color encodings.

![YCbCr vs RGB vs Wii menu vs HBC](wii-rgb-vs-yuyv422.png "YCbCr vs RGB vs Wii menu vs HBC")

The two source images are the overall darker Wii settings menu (top row) and the full screen [Homebrew Channel][wiibrew-hbc] (bottom row).
Note how the overall brightness and colors stay the same for YCbCr, but significantly changes for RGB between the two images.
In particular the RGB capture appears brighter for the darker image and vice versa.
This suggests some sort of [AGC][wiki-agc] is happening here, which is surprising considering the HDMI delivers pixels with absolute known ranges.
For both RGB and YCbCr there is a significant color shift / error in the water rendering of the HBC.
(See [here][wiibrew-hbc] to compare with the original logo and tone).

## Video recording

All the above capture were essentially still frames from a video stream, representing the quality of a video capture.
Due to the previously seen severe quality problems no detailed video recording tests were performed.
However a quick recording of about 5 minutes with 1080p60 showed no problems with the video recording itself.

## Audio quality

Due to the severe quality problems of the video section no detailed audio tests were performed.
A quick recording from the ALSA PCM device worked as expected.

## Conclusion

The general aspects, like UVC support and range of supported input formats, are a good start.
However the [AGC][wiki-agc] that cannot be turned off, will make it very hard to correct the color reproduction problems via the settings.
This is compounded by a low quality scaling filter, causing heavy ringing, that also cannot be disabled.
So the hardware seems entirely capable of coping with the bandwidth but is ultimately hampered by the firmware/software running on it.
Besides fixing the wrong color mapping, the key missing features are:
- Ability to entirely disable any scaling. E.g. a 720x480 (480p DTV) image captured at 800x600 would be padded with black borders to fill the image.
- Ability to entirely disable any [AGC][wiki-agc]

For some additional suggestions on alternatives, have a look [here](https://www.reddit.com/r/linux/comments/c5s5ml/i_tested_4_linuxcompatible_usb_30_hdmi_capture/)

[wiki-agc]: https://en.wikipedia.org/wiki/Automatic_gain_control
[wiibrew-hbc]: https://wiibrew.org/wiki/Homebrew_Channel
