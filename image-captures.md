# List of captured images

Images were captured using [snap.sh](snap.sh), like so

```bash
./snap.sh ossc-testpattern-rgb yuyv422 1920x1080
```

For reference the OSSC menu is visible, its background should be R=0,G=0,B=255.
For the Wii sources a pink broder was added for reference, this should be R=255,G=0,B=255.

## OSSC test pattern output at 480p60

- [OSSC outputing in RGB, box streaming as YUYV 4:2:2 in 1080p](ossc-testpattern-rgb-yuyv422-1920x1080.png)
- [OSSC outputing in YCbCr444, box streaming as YUYV 4:2:2 in 1080p](ossc-testpattern-ycbcr-yuyv422-1920x1080.png)
- [OSSC outputing in RGB, box streaming as Motion-JPEG in 1080p](ossc-testpattern-rgb-mjpeg-1920x1080.png)
- [OSSC outputing in RGB, box streaming as YUYV 4:2:2 in 640x480 VGA](ossc-testpattern-rgb-yuyv422-640x480.png)

## Wii settings menu at 480p60

- [OSSC outputing in RGB, box streaming as YUYV 4:2:2 in 1080p](wii-menu-rgb-yuyv422-1920x1080.png)
- [OSSC outputing in YCbCr444, box streaming as YUYV 4:2:2 in 1080p](wii-menu-ycbcr-yuyv422-1920x1080.png)

## Wii HBC channel at 480p60

- [OSSC outputing in RGB, box streaming as YUYV 4:2:2 in 1080p](wii-hbc-rgb-yuyv422-1920x1080.png)
- [OSSC outputing in YCbCr444, box streaming as YUYV 4:2:2 in 1080p](wii-hbc-ycbcr-yuyv422-1920x1080.png)
