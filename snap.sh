#!/bin/bash

image="$1"
format="$2"
size="$3"

if [[ "$image" == "" ]] ; then
	echo "need image"
	exit 1
fi

if [[ "$format" == "" ]] ; then
	format=yuyv422
fi

if [[ "$size" == "" ]] ; then
	size=1920x1080
fi


ffmpeg \
	-y \
	-f v4l2 -input_format $format -framerate 60 -video_size $size -i /dev/video0 \
	-frames:v 1 "${image}-${format}-${size}.png"
